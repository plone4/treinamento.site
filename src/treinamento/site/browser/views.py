# -*- coding: utf-8 -*-
from plone import api
from plone.dexterity.browser.view import DefaultView
from Products.Five.browser import BrowserView
from operator import itemgetter


class BannersView(BrowserView):
    """ Return images for banner of HomePage
    """

    def get_sliders(self):
        results = []
        folder_slider = getattr(self.context,'slider-images',None)
        if folder_slider:
            banners = api.content.find(context=folder_slider,
                                       portal_type='banner',
                                       sort_on="getObjPositionInParent",)
            for banner in banners:
                banner = banner.getObject()
                if banner.target:
                    target = '_blank',
                else:
                    target = ''
                results.append({
                    'titulo': banner.Title(),
                    'descricao': banner.Description(),
                    'link': banner.link,
                    'target': target,
                    'url': banner.absolute_url(),
                    })
        return results

class TalkView(DefaultView):
    """ The default view for talks
    """


class TalkListView(BrowserView):
    """ A list of talks
    """

    def talks(self):
        results = []
        brains = api.content.find(context=self.context, portal_type='talk')
        for brain in brains:
            talk = brain.getObject()
            results.append({
                'title': brain.Title,
                'description': brain.Description,
                'url': brain.getURL(),
                'audience': ', '.join(talk.audience),
                'type_of_talk': talk.type_of_talk,
                'speaker': talk.speaker,
                'room': talk.room,
                'uuid': brain.UID,
                })
        return results