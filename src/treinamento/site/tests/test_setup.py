# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from plone import api
from treinamento.site.testing import TREINAMENTO_SITE_INTEGRATION_TESTING  # noqa

import unittest


class TestSetup(unittest.TestCase):
    """Test that treinamento.site is properly installed."""

    layer = TREINAMENTO_SITE_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if treinamento.site is installed."""
        self.assertTrue(self.installer.isProductInstalled(
            'treinamento.site'))

    def test_browserlayer(self):
        """Test that ITreinamentoSiteLayer is registered."""
        from treinamento.site.interfaces import (
            ITreinamentoSiteLayer)
        from plone.browserlayer import utils
        self.assertIn(
            ITreinamentoSiteLayer,
            utils.registered_layers())


class TestUninstall(unittest.TestCase):

    layer = TREINAMENTO_SITE_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')
        self.installer.uninstallProducts(['treinamento.site'])

    def test_product_uninstalled(self):
        """Test if treinamento.site is cleanly uninstalled."""
        self.assertFalse(self.installer.isProductInstalled(
            'treinamento.site'))

    def test_browserlayer_removed(self):
        """Test that ITreinamentoSiteLayer is removed."""
        from treinamento.site.interfaces import \
            ITreinamentoSiteLayer
        from plone.browserlayer import utils
        self.assertNotIn(
           ITreinamentoSiteLayer,
           utils.registered_layers())
