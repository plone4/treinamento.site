# -*- coding: utf-8 -*-
from plone.app.layout.viewlets import ViewletBase
from plone import api

class RodapeViewlet(ViewletBase):
    """ Retorna os links do rodapé """

    def get_footer(self):
        results = []
        rodape = getattr(self.context,'rodape',None)
        if rodape != None:
            folders = api.content.find(context=rodape,
                                       depth=1,
                                       portal_type="Folder")
            for folder in folders:
                D={}
                D['titulo'] = folder.Title
                D['links'] = []
                for link in folder.getObject().objectValues():
                    D['links'].append({'titulo_url':link.Title(),
                                       'url':link.remoteUrl
                    })
                results.append(D)
        return results