# -*- coding: utf-8 -*-
from plone import api

import logging

default_profile = 'profile-treinamento.site:default'
logger = logging.getLogger(__name__)


def upgrade_site(setup):
    setup.runImportStepFromProfile(default_profile, 'typeinfo')
    portal = api.portal.get()
    # Create a folder 'Administracao Site' if needed
    if 'adm-site' not in portal:
        adm_folder = api.content.create(
            container=portal,
            type='Folder',
            id='adm-site',
            title=u'Administração do Site',
            description=u'Pasta com conteudo de configuração do site como banners, redes sociais, etc...')
        adm_folder.exclude_from_nav = True
        adm_folder.reindexObject()
    else:
        adm_folder = portal['adm-site']

    # Create folder 'Banners' inside 'Administracao Site' if needed
    if 'banners' not in adm_folder:
        banners_folder = api.content.create(
            container=adm_folder,
            type='Folder',
            id='banners',
            title=u'Banners')
    else:
        banners_folder = adm_folder['banners']
    banners_url = banners_folder.absolute_url()

    # Find all banners
    brains = api.content.find(portal_type='banner')
    for brain in brains:
        if banners_url in brain.getURL():
            # Skip if the banner is already somewhere inside the target-folder
            continue
        obj = brain.getObject()
        logger.info('Moving {} to {}'.format(
            obj.absolute_url(), banners_folder.absolute_url()))
        # Move banner to the folder '/adm-site/banners'
        api.content.move(
            source=obj,
            target=banners_folder,
            safe_id=True)